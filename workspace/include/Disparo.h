//Autor: Marco Iglesias
// Disparo.h: interface for the Disparo class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "Esfera.h"
#include "Vector2D.h"

class Disparo : public Esfera
{ 
public:
	Disparo();
	Disparo(Vector2D pos,float v);

	virtual ~ Disparo();

	Vector2D  centro;
	Vector2D velocidad;
	float radio;

	virtual	void Mueve(float t);
	virtual	void Dibuja();
};
