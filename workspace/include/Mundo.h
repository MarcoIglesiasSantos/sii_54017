//Autor: Marco Iglesias
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <fcntl.h>
#include <unistd.h>
#include "Disparo.h"
#include "Esfera.h"
#include "DatosMemCompartida.h"
#include <fcntl.h>
#include <unistd.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMundo  
{ 
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	
	int fd;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
////////////////
	std::vector<Disparo*>disparos;	
///////////////////
	int puntos1;
	int puntos2;
///////////////////
	DatosMemCompartida datos;
	DatosMemCompartida* pdatos;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
