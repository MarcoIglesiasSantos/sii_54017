// Disparo.cpp: implementation of the Esfera class
//
//////////////////////////////////////

#include "../include/Disparo.h"
#include "../include/glut.h"
///////////////////////////
// Construction/Destruction
////////////////////////////

Disparo::Disparo(Vector2D pos, float v){
	centro=pos;
        radio=0.01f;
        velocidad.x=v;
        velocidad.y=0;
}

Disparo::Disparo(){

}
Disparo::~Disparo(){

}


void Disparo::Mueve(float t){

centro=centro+velocidad*t;
}

void Disparo::Dibuja(){
	glColor3ub(255,255,0);
        glEnable(GL_LIGHTING);
        glPushMatrix();
        glTranslatef(centro.x,centro.y,0);
        glutSolidSphere(radio,15,15);
        glPopMatrix();

}
